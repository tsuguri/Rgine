
extern crate glium;
extern crate rand;
extern crate std;

use window::engine::Facade as Facade;
use std::vec::*;
use std::time::Instant;
use std::option::Option;
use input::InputState;
use input::keyboard_input::KeyCode;
use renderer::data_types::*;
use renderer::camera::*;
use utils::files::*;
use math::vector::Vector3;
use window::engine::App;

fn random_bezier_patch(
	min: [f32; 2],
	max: [f32; 2],
	display: &glium::backend::Facade,
) -> Box<BezierPatch> {
	let p00 = Vector3::new(min[0], 0.0, min[1]);
	let p01 = Vector3::new(min[0], 0.0, min[1] + (max[1] - min[1]) / 3.0);
	let p02 = Vector3::new(min[0], 0.0, min[1] + 2.0 * (max[1] - min[1]) / 3.0);
	let p03 = Vector3::new(min[0], 0.0, max[1]);
	let p10 = Vector3::new(min[0] + (max[0] - min[0]) / 3.0, 0.0, min[1]);
	let p11 = Vector3::new(
		min[0] + (max[0] - min[0]) / 3.0,
		rand::random::<f32>() * 2.0 - 1.0,
		min[1] + (max[1] - min[1]) / 3.0,
	);
	let p12 = Vector3::new(
		min[0] + (max[0] - min[0]) / 3.0,
		rand::random::<f32>() * 2.0 - 1.0,
		min[1] + 2.0 * (max[1] - min[1]) / 3.0,
	);
	let p13 = Vector3::new(min[0] + (max[0] - min[0]) / 3.0, 0.0, max[1]);
	let p20 = Vector3::new(min[0] + 2.0 * (max[0] - min[0]) / 3.0, 0.0, min[1]);
	let p21 = Vector3::new(
		min[0] + 2.0 * (max[0] - min[0]) / 3.0,
		rand::random::<f32>() * 2.0 - 1.0,
		min[1] + (max[1] - min[1]) / 3.0,
	);
	let p22 = Vector3::new(
		min[0] + 2.0 * (max[0] - min[0]) / 3.0,
		rand::random::<f32>() * 2.0 - 1.0,
		min[1] + 2.0 * (max[1] - min[1]) / 3.0,
	);
	let p23 = Vector3::new(min[0] + 2.0 * (max[0] - min[0]) / 3.0, 0.0, max[1]);
	let p30 = Vector3::new(max[0], 0.0, min[1]);
	let p31 = Vector3::new(max[0], 0.0, min[1] + (max[1] - min[1]) / 3.0);
	let p32 = Vector3::new(max[0], 0.0, min[1] + 2.0 * (max[1] - min[1]) / 3.0);
	let p33 = Vector3::new(max[0], 0.0, max[1]);


	let shape = vec![
		PositionVertex::new(p00),
		PositionVertex::new(p01),
		PositionVertex::new(p02),
		PositionVertex::new(p03),
		PositionVertex::new(p10),
		PositionVertex::new(p11),
		PositionVertex::new(p12),
		PositionVertex::new(p13),
		PositionVertex::new(p20),
		PositionVertex::new(p21),
		PositionVertex::new(p22),
		PositionVertex::new(p23),
		PositionVertex::new(p30),
		PositionVertex::new(p31),
		PositionVertex::new(p32),
		PositionVertex::new(p33),
	];
	let verts = glium::VertexBuffer::new(display, &shape).unwrap();
	let verts2 = glium::VertexBuffer::new(display, &shape).unwrap();
	let indic = vec![
		0u32,
		1,
		2,
		3,
		7,
		6,
		5,
		4,
		8,
		9,
		10,
		11,
		15,
		14,
		13,
		12,
		8,
		4,
		0,
		1,
		5,
		9,
		13,
		14,
		10,
		6,
		2,
		3,
		7,
		11,
		15,
	];
	let indic2 = vec![0u32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
	let indices =
		glium::index::IndexBuffer::new(display, glium::index::PrimitiveType::LineStrip, &indic)
			.unwrap();
	let surf_idic = glium::index::IndexBuffer::new(
		display,
		glium::index::PrimitiveType::Patches {
			vertices_per_patch: 16,
		},
		&indic2,
	).unwrap();
	Box::new(BezierPatch {
		mesh: Model::new(verts, indices),
		surface: Model::new(verts2, surf_idic),
		x: 0,
		y: 0,
	})
}

fn random_bspline(
	min: [f32; 2],
	max: [f32; 2],
	div: usize,
	display: &glium::backend::Facade,
) -> Vec<Box<BezierPatch>> {
	let mut vec = Vec::with_capacity(div + 3);
	for _ in 0..div + 3 {
		vec.push(Vec::with_capacity(div + 3));
	}

	let x_step = (max[0] - min[0]) as f32 / (div + 2) as f32;
	let y_step = (max[1] - min[1]) as f32 / (div + 2) as f32;

	for i in 0..div + 3 {
		for j in 0..div + 3 {
			vec[i].push(Vector3::new(
				min[0] + i as f32 * x_step,
				rand::random::<f32>() * 2.0 - 1.0,
				min[1] + j as f32 * y_step,
			));
		}
	}

	let mut meshes = Vec::with_capacity(div * div);
	for i in 0..div {
		for j in 0..div {
			let shape = vec![
				PositionVertex::new(vec[i][j]),
				PositionVertex::new(vec[i][j + 1]),
				PositionVertex::new(vec[i][j + 2]),
				PositionVertex::new(vec[i][j + 3]),
				PositionVertex::new(vec[i + 1][j]),
				PositionVertex::new(vec[i + 1][j + 1]),
				PositionVertex::new(vec[i + 1][j + 2]),
				PositionVertex::new(vec[i + 1][j + 3]),
				PositionVertex::new(vec[i + 2][j]),
				PositionVertex::new(vec[i + 2][j + 1]),
				PositionVertex::new(vec[i + 2][j + 2]),
				PositionVertex::new(vec[i + 2][j + 3]),
				PositionVertex::new(vec[i + 3][j]),
				PositionVertex::new(vec[i + 3][j + 1]),
				PositionVertex::new(vec[i + 3][j + 2]),
				PositionVertex::new(vec[i + 3][j + 3]),
			];
			let verts = glium::VertexBuffer::new(display, &shape).unwrap();
			let verts2 = glium::VertexBuffer::new(display, &shape).unwrap();
			let indic = vec![
				0u32,
				1,
				2,
				3,
				7,
				6,
				5,
				4,
				8,
				9,
				10,
				11,
				15,
				14,
				13,
				12,
				8,
				4,
				0,
				1,
				5,
				9,
				13,
				14,
				10,
				6,
				2,
				3,
				7,
				11,
				15,
			];
			let indic2 = vec![0u32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
			let indices = glium::index::IndexBuffer::new(
				display,
				glium::index::PrimitiveType::LineStrip,
				&indic,
			).unwrap();
			let surf_indic = glium::index::IndexBuffer::new(
				display,
				glium::index::PrimitiveType::Patches {
					vertices_per_patch: 16,
				},
				&indic2,
			).unwrap();
			meshes.push(Box::new(BezierPatch {
				mesh: Model::new(verts, indices),
				surface: Model::new(verts2, surf_indic),
				x: i as i32,
				y: j as i32,
			}));
		}
	}
	meshes
}

struct BezierPatch {
	pub x: i32,
	pub y: i32,
	pub mesh: Model<PositionVertex>,
	pub surface: Model<PositionVertex>,
}

pub struct BsplineApplication {
	data: Option<BsplineData>,
}

struct BsplineData {
	diffuse_texture: glium::Texture2d,
	normal_texture: glium::Texture2d,
	height_texture: glium::Texture2d,
	shader_program: glium::Program,
	outline_program: glium::Program,
	bspline_program: glium::Program,
	bezier: Box<BezierPatch>,
	bezier2: Box<BezierPatch>,
	multiple_beziers: std::vec::Vec<Box<BezierPatch>>,
	bspline: std::vec::Vec<Box<BezierPatch>>,
	camera: Camera,
	t: f32,
	outer_div: f32,
	inner_div: f32,
	mode: i32,
	now: Instant,
	filled: bool,
	draw_polygon: bool,
	polygon_mode: glium::draw_parameters::PolygonMode
}

impl BsplineData {
	fn new(display: &Facade) -> BsplineData {
		let diffuse_texture = load_texture(display, &String::from("res/diffuse.jpg"));
		let normal_texture = load_texture(display, &String::from("res/normals.jpg"));
		let height_texture = load_texture(display, &String::from("res/height.jpg"));

		let triangle_vert = load_file_content(&String::from("res/Shaders/triangleVert.vert"));
		let outline_vert = load_file_content(&String::from("res/Shaders/outlineVert.vert"));
		let triangle_frag = load_file_content(&String::from("res/Shaders/triangleFrag.frag"));
		let outline_frag = load_file_content(&String::from("res/Shaders/outlineFrag.frag"));
		let triangle_tess_control =
			load_file_content(&String::from("res/Shaders/triangleTessControl.tesc"));
		let bspline_tess_control =
			load_file_content(&String::from("res/Shaders/bsplineTessControl.tesc"));
		let triangle_tess_eval = load_file_content(&String::from("res/Shaders/triangleTessEval.tese"));
		let bspline_tess_eval = load_file_content(&String::from("res/Shaders/bsplineTessEval.tese"));

		let shader_source = glium::program::SourceCode {
			vertex_shader: &triangle_vert,
			tessellation_control_shader: Option::Some(&triangle_tess_control),
			tessellation_evaluation_shader: Option::Some(&triangle_tess_eval),
			geometry_shader: Option::None,
			fragment_shader: &triangle_frag,
		};
		let shader_program = glium::Program::new(display, shader_source).unwrap();
		let outline_program =
			glium::Program::from_source(display, &outline_vert, &outline_frag, Option::None).unwrap();

		let shader_source = glium::program::SourceCode {
			vertex_shader: &triangle_vert,
			tessellation_control_shader: Option::Some(&bspline_tess_control),
			tessellation_evaluation_shader: Option::Some(&bspline_tess_eval),
			geometry_shader: Option::None,
			fragment_shader: &triangle_frag,
		};
		let bspline_program = glium::Program::new(display, shader_source).unwrap();
		let bezier = random_bezier_patch([0.0, 0.0], [2.0, 2.0], display);
		let bezier2 = random_bezier_patch([0.0, 0.0], [2.0, 2.0], display);
		let multiple_beziers = vec![
			random_bezier_patch([-4.0, -4.0], [-2.0, -2.0], display),
			random_bezier_patch([-2.0, -4.0], [0.0, -2.0], display),
			random_bezier_patch([0.0, -4.0], [2.0, -2.0], display),
			random_bezier_patch([2.0, -4.0], [4.0, -2.0], display),
			random_bezier_patch([-4.0, -2.0], [-2.0, 0.0], display),
			random_bezier_patch([-2.0, -2.0], [0.0, 0.0], display),
			random_bezier_patch([0.0, -2.0], [2.0, 0.0], display),
			random_bezier_patch([2.0, -2.0], [4.0, 0.0], display),
			random_bezier_patch([-4.0, 0.0], [-2.0, 2.0], display),
			random_bezier_patch([-2.0, 0.0], [0.0, 2.0], display),
			random_bezier_patch([0.0, 0.0], [2.0, 2.0], display),
			random_bezier_patch([2.0, 0.0], [4.0, 2.0], display),
			random_bezier_patch([-4.0, 2.0], [-2.0, 4.0], display),
			random_bezier_patch([-2.0, 2.0], [0.0, 4.0], display),
			random_bezier_patch([0.0, 2.0], [2.0, 4.0], display),
			random_bezier_patch([2.0, 2.0], [4.0, 4.0], display),
		];
		let bspline = random_bspline([-4.0, -4.0], [4.0, 4.0], 8, display);

		let camera = Camera::new(
			-45.0,
			30.0,
			Vector3::new(0.0, 0.0, 0.0),
			3.0,
			3.14 / 5.0,
			0.1,
			100.0,
			3.0 / 4.0,
		);

		BsplineData {
			diffuse_texture: diffuse_texture,
			normal_texture: normal_texture,
			height_texture: height_texture,
			shader_program: shader_program,
			outline_program: outline_program,
			bspline_program: bspline_program,
			bezier: bezier,
			bezier2: bezier2,
			multiple_beziers: multiple_beziers,
			camera: camera,
			bspline: bspline,
			t: -0.5,
			outer_div: 3.0,
			inner_div: 3.0,
			mode: 0,
			now: Instant::now(),
			filled: true,
			draw_polygon: true,
			polygon_mode: glium::draw_parameters::PolygonMode::Fill
		}
	}
}

impl BsplineApplication {
	pub fn new() -> BsplineApplication {
		BsplineApplication { data: None }
	}
}

impl App for BsplineApplication {
	fn initialize(&mut self, display: &Facade) {
		self.data = Some(BsplineData::new(display))
	}

	fn run_logic(&mut self, input: &InputState, should_end: &mut bool) {
		if input.keyboard.is_button_down(KeyCode::Escape) {
			*should_end = true;
		}
		match self.data {
			Some(ref mut data) => {
				let dt = data.now.elapsed();
				let duration = dt.as_secs() as f32 + dt.subsec_nanos() as f32 / 1_000_000_000.0;
				data.now = Instant::now();
				data.t += duration;

				let mouse_dt = input.mouse.get_mouse_move();
				if input.mouse.right_button_down() {
					data.camera.modify_radius(mouse_dt[1] / 10.0);
				}
				if input.mouse.left_button_down() {
					data.camera.modify_x_angle(-mouse_dt[1] / 3.0);
					data.camera.modify_y_angle(-mouse_dt[0] / 3.0);
				}

				if input.keyboard.button_pressed_in_last_frame(KeyCode::P) {
					data.inner_div += 1.0;
				}
				if input.keyboard.button_pressed_in_last_frame(KeyCode::O) {
					data.inner_div -= 1.0;
				}
				if input.keyboard.button_pressed_in_last_frame(KeyCode::L) {
					data.outer_div += 1.0;
				}
				if input.keyboard.button_pressed_in_last_frame(KeyCode::K) {
					data.outer_div -= 1.0;
				}
				if input.keyboard.button_pressed_in_last_frame(KeyCode::N) {
					data.mode += 1;
				}
				if input.keyboard.button_pressed_in_last_frame(KeyCode::G) {
					data.draw_polygon = !data.draw_polygon;
				}
				if input.keyboard.button_pressed_in_last_frame(KeyCode::F) {
					data.filled = !data.filled;
					if data.filled {
						data.polygon_mode =
							glium::draw_parameters::PolygonMode::Fill;
					} else {
						data.polygon_mode =
							glium::draw_parameters::PolygonMode::Line;
					}
				}
			}
			None => (),
		}
	}
	fn run_render(&mut self, target: &mut glium::Frame) {
		use glium::Surface;
		target.clear_color(0.3, 0.6, 0.7, 1.0);
		target.clear_depth(1.0); 
		match self.data {
			Some(ref mut data) => {
				let light_pos = Vector3::<f32>::new(data.t.sin(), 2.0, data.t.cos());
				let view_mat = data.camera.get_view_matrix().transposed();
				let perspective_mat = data.camera.get_projection_matrix().transposed();
				let mat = view_mat * perspective_mat;
				let uniforms = uniform!{ t: data.t,
					lightPosition : light_pos.content(),
					cameraPosition : data.camera.get_position().content(),
					inner: data.inner_div,
					outer: data.outer_div,
					texDiffuse: &data.diffuse_texture,
					texNormal: &data.normal_texture,
					texHeight: &data.height_texture,
					viewModel: view_mat.content(),
					perspective : perspective_mat.content()};
				let outline_params = glium::DrawParameters {
					polygon_mode: glium::draw_parameters::PolygonMode::Line,
					depth: glium::Depth {
						test: glium::DepthTest::IfLess,
						write: true,
						..Default::default()
					},
					..Default::default()
				};
				let surface_params = glium::DrawParameters {
					polygon_mode: glium::draw_parameters::PolygonMode::Fill,
					 depth: glium::Depth {
					 	test: glium::DepthTest::IfLess,
					 	write: true,
					 	..Default::default()
					 },
					..Default::default()
				};
				match data.mode {
					0 => {
						if data.draw_polygon {
							target
								.draw(
									data.bezier.mesh.get_vertices(),
									data.bezier.mesh.get_indices(),
									&data.outline_program,
									&uniform!{t: data.t, mat: mat.content()},
									&outline_params,
								)
								.unwrap();
						}
						target
							.draw(
								data.bezier.surface.get_vertices(),
								data.bezier.surface.get_indices(),
								&data.shader_program,
								&uniforms,
								&surface_params
							)
							.unwrap();
					}
					1 => {
						if data.draw_polygon {
							target
								.draw(
									data.bezier2.mesh.get_vertices(),
									data.bezier2.mesh.get_indices(),
									&data.outline_program,
									&uniform!{t: data.t, mat: mat.content()},
									&outline_params,
								)
								.unwrap();
						}
						target
							.draw(
								data.bezier2.surface.get_vertices(),
								data.bezier2.surface.get_indices(),
								&data.shader_program,
								&uniforms,
								&surface_params,
							)
							.unwrap();
					}
					2 => for bez in &data.multiple_beziers {
						if data.draw_polygon {
							target
								.draw(
									bez.mesh.get_vertices(),
									bez.mesh.get_indices(),
									&data.outline_program,
									&uniform!{t: data.t, mat: mat.content()},
									&outline_params,
								)
								.unwrap();
						}
						target
							.draw(
								bez.surface.get_vertices(),
								bez.surface.get_indices(),
								&data.shader_program,
								&uniforms,
								&surface_params,
							)
							.unwrap();
					},
					3 => for bspl in &data.bspline {
						if data.draw_polygon {
							target
								.draw(
									bspl.mesh.get_vertices(),
									bspl.mesh.get_indices(),
									&data.outline_program,
									&uniform!{t: data.t, mat: mat.content()},
									&outline_params,
								)
								.unwrap();
						}
						target
							.draw(
								bspl.surface.get_vertices(),
								bspl.surface.get_indices(),
								&data.bspline_program,
								&uniform!{ t: data.t,
								lightPosition : light_pos.content(),
								cameraPosition : data.camera.get_position().content(),
								inner: data.inner_div,
								outer: data.outer_div,
								texDiffuse: &data.diffuse_texture,
								texNormal: &data.normal_texture,
								texHeight: &data.height_texture,
								viewModel: view_mat.content(),
								perspective : perspective_mat.content(),
								x: bspl.x,
								y: bspl.y},
								&surface_params,
							)
							.unwrap();
					},
					_ => data.mode = 0,
				}
			}
			None => (),
		};
	}
}