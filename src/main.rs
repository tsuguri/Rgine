
#[macro_use]
extern crate glium;
#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;
mod input;
mod renderer;
mod math;
mod utils;
mod window;
mod apps;
mod engine;

use apps::bspline_app::BsplineApplication;


fn main() {
	let mut conf = window::engine::WindowConfiguration::new();
	conf.window_name = String::from("shiet");
	let mut newapp = engine::rgine::RgineApp::new(String::from("res/config.json"));
	let mut app = BsplineApplication::new();
	let mut engine = window::engine::Engine::new();
	engine.run(&mut app);
}
