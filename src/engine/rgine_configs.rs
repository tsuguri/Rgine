fn default_window_name() -> String {String::from("Rgine Window")}
fn default_window_height() -> i32 {720}
fn default_window_width() -> i32 {1280}
fn default_postprocessing_file() -> Option<String> {None}
fn default_resource_file() -> Option<String> {None}
#[derive(Serialize, Deserialize, Debug)]
pub struct RgineConfig {
    #[serde(default="default_window_name")]
    pub window_name: String,
    #[serde(default="default_window_height")]
    pub window_height: i32,
    #[serde(default="default_window_width")]
    pub window_width: i32,
    #[serde(default="default_resource_file")]
    pub resource_file_path: Option<String>,
    #[serde(default="default_postprocessing_file")]
    pub postprocessing_file_path: Option<String>,
}